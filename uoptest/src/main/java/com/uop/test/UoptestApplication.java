package com.uop.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UoptestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UoptestApplication.class, args);
	}
}
