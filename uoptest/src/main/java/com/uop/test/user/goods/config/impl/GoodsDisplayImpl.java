package com.uop.test.user.goods.config.impl;

public class GoodsDisplayImpl {
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getListOrder() {
		return listOrder;
	}
	public void setListOrder(String listOrder) {
		this.listOrder = listOrder;
	}
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubhead() {
		return subhead;
	}
	public void setSubhead(String subhead) {
		this.subhead = subhead;
	}
	public String getActivityStartTimeStr() {
		return activityStartTimeStr;
	}
	public void setActivityStartTimeStr(String activityStartTimeStr) {
		this.activityStartTimeStr = activityStartTimeStr;
	}
	public String getActivityEndTimeStr() {
		return activityEndTimeStr;
	}
	public void setActivityEndTimeStr(String activityEndTimeStr) {
		this.activityEndTimeStr = activityEndTimeStr;
	}
	public String getActivityAddress() {
		return activityAddress;
	}
	public void setActivityAddress(String activityAddress) {
		this.activityAddress = activityAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDisplayType() {
		return displayType;
	}
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
	private String position             ;
	private String listOrder            ;
	private String picPath              ;
	private String itemId               ;
	private String title                ;
	private String subhead              ;
	private String activityStartTimeStr ;
	private String activityEndTimeStr   ;
	private String activityAddress      ;
	private String status               ;
	private String displayType          ;
}
